/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import firebase from 'firebase';
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {

  componentWillMount(){
    var config = {
      apiKey: "AIzaSyBOPUpQQP6Wj1vIZmmHRY4Bxjhn6gUJJA8",
      authDomain: "configuracaofirebasereac-55c4d.firebaseapp.com",
      databaseURL: "https://configuracaofirebasereac-55c4d.firebaseio.com",
      projectId: "configuracaofirebasereac-55c4d",
      storageBucket: "configuracaofirebasereac-55c4d.appspot.com",
      messagingSenderId: "906160308563"
    };
    firebase.initializeApp(config);
  }


  // saveData(){
  //   var employees = firebase.database().ref("funcionarios");
  //   employees.push().set(
  //     {
  //       nome: "Rodrigo",
  //       sobrenome: "Toledo"
  //     }
  //   );
  //   // database.ref("pontuacao").remove();
  // }

  // listData(){
  //   var points = firebase.database().ref("pontuacao");
  //   points.on('value', (snapshot) => {
  //     var points = snapshot.val();
  //     this.setState({points});
  //   });
  // }

  saveUser(){
    var email = 'agc.rodrigo@gmail.com';
    var password = 'asdqwe1234';

    const user = firebase.auth();
    user.createUserWithEmailAndPassword(email,password).
      catch(
        (error) => {
          alert(error.message);
        }
      );
  }

  checkUserIsAuth(){
    const user = firebase.auth();
    user.onAuthStateChanged(
      (currentUser) => {
        if(currentUser){
          alert('Usuário logado')
        }else{
          alert('Usuário NÃO ESTÁ logado')
        }
      }
    );
    // const currentUser = user.currentUser;

    // if(currentUser){
    //   alert('Usuário logado')
    // }else{
    //   alert('Usuário NÃO ESTÁ logado')
    // }


  }

  logout(){
    const user = firebase.auth();
    user.signOut();
  }

  login(){
    var email = 'agc.rodrigo@gmail.com';
    var password = 'asdqwe1234';

    const user = firebase.auth();
    user.signInWithEmailAndPassword(email,password);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Olá
        </Text>
        <Button
        title="Salvar usuário"
        accessibilityLabel="Salvar usuário"
        onPress={ ()=>{ this.saveUser(); } }
        color="#841584"
        />

        <Button
        title="Checar autenticação"
        accessibilityLabel="Checar autenticação"
        onPress={ ()=>{ this.checkUserIsAuth(); } }
        color="#841584"
        />

        <Button
        title="Deslogar"
        accessibilityLabel="Deslogar"
        onPress={ ()=>{ this.logout(); } }
        color="#841584"
        />

        <Button
        title="Autenticar"
        accessibilityLabel="Autenticar"
        onPress={ ()=>{ this.login(); } }
        color="#841584"
        />
        <Text style={styles.instructions}>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
